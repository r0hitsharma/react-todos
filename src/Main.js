import React, { Component } from 'react';
import TodoEntry from "./TodoEntry";
import TodoList from "./TodoList";

class Main extends Component {
	render() {
		// console.log(this.props)
		const { addNewTodo, todos, switchTodo, deleteItem } = this.props

	  return (
	    <div className="App">
	      <TodoEntry add={ addNewTodo }/>
	      <TodoList
		      todos={ todos }
		      handleChange={ switchTodo }
		      handleDelete={ deleteItem }
		    />
	    </div>
	  );
	}
}

export default Main;