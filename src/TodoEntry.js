import React, { Component } from 'react';

class TodoEntry extends Component {
	handleSubmit(e){
		e.preventDefault()
		// console.log(e.target.desc.value)

		const { add } = this.props
		add(e.target.desc.value)
	}

	render() {
		return (
			<form onSubmit={ this.handleSubmit.bind(this) }>
				<input type="text" name="desc"/>
				<input type="submit" value="Submit"/>
			</form>
		)
	}
}

export default TodoEntry;