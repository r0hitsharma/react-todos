import React, {Component} from 'react'

class TodoItem extends Component {
	render() {
		const { todo, handleChange, handleDelete } = this.props
		const { desc, time, done } = todo
		return (
			<li>
				<input type="checkbox" checked={ done } onChange={ handleChange.bind(null, time) } />
				<a href={ `/todo/${time}` }>{ desc }</a>
				<button className="delete" onClick={ handleDelete.bind(null, time) }></button>
			</li>
		)
	}
}

export default TodoItem