import React, {Component} from 'react'

class TodoEdit extends Component {
	constructor (props){
		super(props)

		const { match, todos } = props
		const time = match.params.time
		const todo = todos.find(t => t.time === parseInt(time))
		const { desc } = todo

		this.state = {
			desc,
			time: todo.time
		}
	}

	handleSubmit(history, e) {
		e.preventDefault()
		const { desc, time } = this.state
		this.props.updateItem(time, desc)
		history.push("/")
	}

	onChange(e) {
		this.setState({
			desc: e.target.value
		})
	}

	render() {
		// console.log(this.props)
		const { desc } = this.state
		const { history } = this.props

		return (
			<form onSubmit={ this.handleSubmit.bind(this, history) }>
				<input type="text" value={ desc } onChange={ this.onChange.bind(this) }/>
				<input type="submit" value="Edit"/>
			</form>
		)
	}
}

export default TodoEdit