import React, { Component } from 'react';
import TodoItem from './TodoItem'

class TodoList extends Component {
	render() {
		const { todos, handleChange, handleDelete } = this.props

		return (<ul>
			{ todos.map(todo =>  <TodoItem
				todo={ todo }
				handleChange={ handleChange }
				key={ todo.time }
				handleDelete={ handleDelete }
			/>)}
		</ul>)
	}
}

export default TodoList;