import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import './App.css';
import Main from "./Main";
import TodoEdit from "./TodoEdit";

class App extends Component {
  // default state
  state = {
    todos: []
  }

  addNewTodo(desc, time=Date.now(), done=false) {
    // console.log(todo)
    this.setState({todos: [...this.state.todos, { desc, time, done }] })
  }

  switchTodo(time) {
    const index = this.state.todos.findIndex(todo => todo.time === time)
    if(index > -1){
      // console.log(index)
      let newTodos = this.state.todos
      newTodos[index].done = !newTodos[index].done
      this.setState({ todos: newTodos })
    }
  }

  deleteItem(time) {
    if(!window.confirm("Are you sture you want to delete this task?"))
      return null

    const index = this.state.todos.findIndex(todo => todo.time === time)
    if(index > -1){
      // console.log(index)
      let newTodos = this.state.todos
      newTodos.splice(index, 1)
      this.setState({ todos: newTodos })
    }
  }

  updateItem(time, desc) {
    const index = this.state.todos.findIndex(todo => todo.time === time)
    if(index > -1){
      console.log(index)
      let newTodos = this.state.todos
      newTodos[index].desc = desc
      this.setState({ todos: newTodos })
    }
  }

  componentDidUpdate(){
    window.localStorage.setItem("todos", JSON.stringify(this.state.todos))
  }

  constructor(props) {
    super(props)

    const todos = window.localStorage.getItem("todos")
    if(todos)
      this.state = { todos: JSON.parse(todos) }
  }

  render() {
    const { todos } = this.state

    return (
      <div className="App">
        <Route
          exact path="/"
          render={() =>
            <Main
              addNewTodo={ this.addNewTodo.bind(this) }
              todos={ todos }
              switchTodo={ this.switchTodo.bind(this) }
              deleteItem={ this.deleteItem.bind(this) }
            />
          }
        />
        <Route
          path="/todo/:time"
          render={props => <TodoEdit
            todos={ todos }
            updateItem={ this.updateItem.bind(this) }
            { ...props }
          /> }
        />
      </div>
    );
  }
}

export default App;
